#lang scribble/manual
@require[@for-label[word
                    racket/base]]

@title{Word}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Types}
@defmodule[word]

Signed/Unsigned Word data type using refinments.

@defthing[Word8 Byte]{
Unsigned 8-bit Word.  Equivalent to a Racket Byte.
}

@defthing[Word16 Integer #:value (Refine (i : Integer) (<= 0 i #xFFFF))]{
Unsigned 16-bit Word.

@defthing[Word32 Integer #:value (Refine (i : Integer) (<= 0 i #xFFFFFFFF))]{
Unsigned 32-bit Word.
}

@defthing[Word64 Integer #:value (Refine (i : Integer) (<= 0 i #xFFFFFFFFFFFFFFFF))]{
Unsigned 64-bit Word.
}

}
